defmodule TimexWeb.IndigloManager do
  use GenServer
  def init(ui) do
    :gproc.reg({:p, :l, :ui_event})
    {:ok, %{ui_pid: ui, st: IndigloOff}}
  end

  def handle_info(:"top-right", %{st: IndigloOff, ui_pid: ui} = state) do
    GenServer.cast(ui, :set_indiglo)
    {:noreply, %{state | st: IndigloOn} }
  end

  def handle_info(:"top-right", %{st: IndigloOn} = state) do
    Process.send_after(self(), Waiting2IndigloOff, 2000)
    {:noreply, %{state | st: Waiting} }
  end

  def handle_info(Waiting2IndigloOff, %{st: Waiting, ui_pid: ui} = state) do
    GenServer.cast(ui, :unset_indiglo)
    {:noreply, state |> Map.put(:st, IndigloOff)}
  end

  def handle_info(_msg, state) do
    {:noreply, state}
  end
end
